import sys
sys.path.append("/usr/local/lib/python2.7/dist-packages")
import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk import pos_tag
from nltk import RegexpParser
from nltk.tag import untag
from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer
from nltk.corpus import wordnet as wn
import string
import re
grammar = """
			SY:{<.|:>}
			NP: {<DT>?<JJ>*<NN.*>}
			P:{<IN>}
			V:{<VB.*>}
			PP:{<P><NP>}
			VP:{<V><NP|PP>+}
			"""

parser = RegexpParser(grammar)

"""
Case_Difference=0.8
Word_Missing=0.5
Order_Difference=0.5
Symbol_Difference=1.5
Tense_Difference=3.4
Noun_Difference=3.4
Others=10
"""
penalty_vector = {'case':0.8,'word_missing':0.5,'order_difference':0.5,'symbol_difference':1.5,
'tense_difference':3.4,'synonyms':3.4,'new_verb':10,'new_noun':10,'others':10}
deviation_vector={}
tags_used = {'CC':'conjunction','DT':'determiner','IN':'preposition','JJ':'adjective','NN':'noun','PRP':'pronoun','RB':'adverb','TO':'to','VB':'verb'}

class Sent_Parser:
	"""
	Parse the sentences
	"""
	def __init__(self,text):
		self.text = text
		self.count = 0

	def process(self):
		"""
		tokenize, removal of stop words and pos tagging of sentence
		"""
		tokens = word_tokenize(self.text)
		tags = pos_tag(tokens)
		return tags
	def parser(self,tags):
		"""
		Parse sentence based on the grammar and return parse tree
		"""
		parser = RegexpParser(grammar)
		tree = parser.parse(tags)
		return tree
			
	def count_words(self,tags):
		"""
		return number of words in tagged sentence
		"""
		return len(tags)
	
	def get_pos(self,tags,pos):
		"""
		return a list of words with 'pos' tag
		"""
		pos_list = untag(filter(lambda x:x[1].startswith(pos),tags))
		return pos_list

class Compare_phrases:
	"""
	Compare original sentence with supplied sentence
	"""
	def __init__(self,tree1,tree2):
		self.tree1 = tree1
		self.tree2 = tree2
		self.noun = []
		self.verb = []
		self.symbol = []
		self.noun_mis = []
		self.verb_mis = []
		self.symbol_mis = []
	

	def noun_compare(self,count_np):
		"""
		Compare noun phrases in original sentence with supplied sentence 
		and return mismatches. Mismatches in tags are also accounted for
		""" 
		for t in self.tree2.subtrees(lambda t:t.label()=='NP'):
			x = 0
			for u in self.tree1.subtrees(lambda u:u.label()=='NP'):
				if t != u: 
					x = x+1
			if x==count_np:
				self.noun.append(t)
		return self.noun

	def noun_missing(self,count_np):
		"""
		Compare noun phrases in original sentence with supplied sentence 
		and return mismatches. Mismatches in tags are also accounted for
		""" 
		for t in self.tree1.subtrees(lambda t:t.label()=='NP'):
			x = count_np
			for u in self.tree2.subtrees(lambda u:u.label()=='NP'):
				if t == u: 
					x = x-1
			if x==count_np:
				self.noun_mis.append(t)
		return self.noun_mis

	def verb_compare(self,count_vp):
		"""
		Compare verb phrases in original sentence with supplied sentence 
		and return mismatches.Mismatches in tags are also accounted for
		"""

		for t in self.tree2.subtrees(lambda t:t.label()=='VP'):
			x = 0
			for u in self.tree1.subtrees(lambda u:u.label()=='VP'):
				if t != u:
					x = x+1
			if x==count_vp:
				self.verb.append(t)
		return self.verb

	def verb_missing(self,count_vp):
		"""
		Compare verb phrases in original sentence with supplied sentence 
		and return mismatches.Mismatches in tags are also accounted for
		"""

		for t in self.tree1.subtrees(lambda t:t.label()=='VP'):
			x = count_vp
			for u in self.tree2.subtrees(lambda u:u.label()=='VP'):
				if t == u:
					x = x-1
			if x==count_vp:
				self.verb_mis.append(t)
		return self.verb_mis

	def symbol_compare(self,count_symbol):
		for t in self.tree2.subtrees(lambda t:t.label()=='SY'):
			x=0
			for u in self.tree1.subtrees(lambda u:u.label()=='SY'):
				if t!=u:
					x = x+1
			if x==count_symbol:
				self.symbol.append(t)
		return self.symbol

	def symbol_missing(self,count_symbol):
		for t in self.tree1.subtrees(lambda t:t.label()=='SY'):
			x=count_symbol
			for u in self.tree2.subtrees(lambda u:u.label()=='SY'):
				if t==u:
					x = x-1
			if x==count_symbol:
				self.symbol_mis.append(t)
		return self.symbol_mis

class Similarity:
	"""
	return similarity score of supplied words based on path_similarity algorith
	"""

	def __init__(self,word1,word2,pos):
		self.word1_low = word1.lower()
		self.word2_low = word2.lower()
		self.word1=wn.morphy(self.word1_low,pos)
		self.word2=wn.morphy(self.word2_low,pos)
		self.pos=pos
	def check_similarity(self):
		#print self.word1
		#print self.word2
		similarity=0
		if self.word1_low==self.word2_low:
			similarity+=1
			return similarity
		if not self.word1 or not self.word2:
			return similarity
		if self.word1==self.word2:
			similarity+=1
		else:
			#syn1 = filter(lambda x:x.pos().__contains__(self.pos),wn.synsets(self.word1,self.pos))
			syn1 = wn.synsets(self.word1,self.pos)
			#print syn1
			j = [i.lemma_names() for i in syn1]
			#print j
			o =  set([n for m in j for n in m])
			#print o
			l = self.word2 in o
			#l = [k.name() for k in syn1 if k.name().__contains__(self.word2)]
			if l: similarity+=1
			else: similarity+=0
		return similarity


def total_score(penalty_vector,deviation_vector):
	#print deviation_vector
	score = [penalty_vector.get(x)*deviation_vector.get(x) for x in deviation_vector.keys()]
	return sum(score)


class Compare:

	def __init__(self,sent1,sent2):
		self.sent1 = sent1
		self.sent2 = sent2

	def create_tree(self,text):
		sent_parsed = Sent_Parser(text)
		sent_tags = sent_parsed.process()
		word_count = sent_parsed.count_words(sent_tags)
		tree = sent_parsed.parser(sent_tags)
		list_noun = sent_parsed.get_pos(sent_tags,'NN')
		list_verb = sent_parsed.get_pos(sent_tags,'VB')
		return tree,sent_tags,list_noun,list_verb,word_count

	def count_phrases(self,tree,label):
		count = 0
		for t in tree.subtrees(lambda t:t.label()==label):
			count+=1
		return count

	def similarity_score(self,pos_list1,pos_list2,pos):
		#print pos_list1
		#print pos_list2
		score = 0
		
		for i in range(0,len(pos_list1)):
			for j in range(0,len(pos_list2)):
				if(pos_list1[i]==pos_list2[j]):
					score +=1
					break

				else:
					sim = Similarity(pos_list1[i],pos_list2[j],pos)
					score += sim.check_similarity()

		#print score
		return score

	def compare_values(self,value1,value2,similarity_score,tags_miss,tags_add,noun_original,verb_original):
		if value1>0:
			deviation_vector['word_missing']=1

		if value2>0:
			noun_list = [m for m,n in tags_add if n.__contains__('NN')]
			verb_list = [m for m,n in tags_add if n.__contains__('VB')]
			other_list = [m for m,n in tags_add if not (n.__contains__('.') or n.__contains__('NN') or \
				n.__contains__(':') or n.__contains__('VB'))]
			if noun_list:
				#print noun_original
				#print noun_list
				min_score = len(noun_list)
				score = similarity_score(noun_original,noun_list,'n')
				#print score
				#print min_score
				if score >= min_score:
					deviation_vector['synonyms']=1
				else:
					deviation_vector['new_noun']=1
			if verb_list:
				min_score = len(verb_list)
				score = similarity_score(verb_original,verb_list,'v')
				#print verb_original
				#print verb_list
				#print score
				#print min_score
				if score >= min_score:
					deviation_vector['tense_difference']=1
				else:
					deviation_vector['new_verb']=1

			if other_list:
				deviation_vector['others']=1
		
		return deviation_vector


			
	def process(self):
		a,b = set(),set()
		tree_original,tags_original,noun_original,verb_original,word_count_original= self.create_tree(self.sent1)
		tree_recalled,tags_recalled,noun_recalled,verb_recalled,word_count_recalled= self.create_tree(self.sent2)
		#tree_original.draw()
		#tree_recalled.draw()
		comparison = Compare_phrases(tree_original,tree_recalled)
		NP_count_original = self.count_phrases(tree_original,'NP')
		VP_count_original = self.count_phrases(tree_original,'VP')
		symbol_count_original = self.count_phrases(tree_original,'SY')
		NP_mismatch = comparison.noun_compare(NP_count_original)
		VP_mismatch = comparison.verb_compare(VP_count_original)
		symbol_mismatch = comparison.symbol_compare(symbol_count_original)
		NP_missing = comparison.noun_missing(NP_count_original)
		VP_missing = comparison.verb_missing(VP_count_original)
		symbol_missing = comparison.symbol_missing(symbol_count_original)
		tags_miss = set(tags_original)-set(tags_recalled)
		tags_add = set(tags_recalled)-set(tags_original)
		missed_word = [(i,j) for (i,j) in tags_miss if not (j.startswith('N') or j.startswith('V'))]
		#print NP_mismatch
		#print VP_mismatch
		#print NP_missing
		#print VP_missing
		#print symbol_missing
		deviation_vector = self.compare_values(len(missed_word),len(tags_add),self.similarity_score,tags_miss,tags_add,noun_original,verb_original)
		
		if symbol_mismatch or symbol_missing:
			deviation_vector['symbol_difference']=1

		if VP_missing or VP_mismatch:
			for y in VP_mismatch:
				a = set([wn.morphy(i[0],'v') for i in y.leaves()])
			for z in VP_missing:
				b = set([wn.morphy(i[0],'v') for i in z.leaves()])
			if a.difference(b) or b.difference(a):
				deviation_vector['order_difference']=1

			deviation_vector['order_difference']=1

		if NP_missing or NP_mismatch:
			for y in NP_mismatch:
				a = set([wn.morphy(i[0],'n') for i in y.leaves()])
			for z in NP_missing:
				b = set([wn.morphy(i[0],'n') for i in z.leaves()])
			if a.difference(b) or b.difference(a):
				deviation_vector['order_difference']=1

		return deviation_vector

	def Compare_sentence(self):
		tokens1 = word_tokenize(self.sent1)
		tokens2 = word_tokenize(self.sent2)
		if self.sent1==self.sent2:
			return deviation_vector
		elif self.sent1.lower()==self.sent2.lower():
			deviation_vector['case']=1
			return deviation_vector
		else:
			tok1_nosymbol = [x for x in tokens1 if x not in string.punctuation]
			tok2_nosymbol = [x for x in tokens2 if x not in string.punctuation]
			if tok1_nosymbol == tok2_nosymbol:
				deviation_vector['symbol_difference']=1
				return deviation_vector
			elif ''.join(tok1_nosymbol).lower()==''.join(tok2_nosymbol).lower():
				deviation_vector['symbol_difference']=1
				deviation_vector['case']=1
				return deviation_vector


class Main:

	def __init__(self,sent1,sent2):
		self.sent1 = sent1
		self.sent2 = sent2
	#print "sent1 "+sent1
	#print "sent2 "+sent2
	def main(self):
		compare = Compare(self.sent1,self.sent2)
		deviation_vector = compare.Compare_sentence() if compare.Compare_sentence() else compare.process()
		overall_penalty = total_score(penalty_vector,deviation_vector)
		#print deviation_vector
		deviation_vector.clear()
		return overall_penalty

	


if __name__=='__main__':
	X= Main("tigress cub is climbing","tigress cub climb").main()
	print X	
	
