from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext

# Create your views here.
from django.views.decorators.csrf import csrf_protect
from django.http import HttpResponseRedirect
from publish.forms import UserForm,LoginForm,PublishForm,ImageuploadForm,\
ImageselectionForm,PenaltyForm,EditPublishForm,EditSubmitForm
from publish.models import User,Image,Publication
#from django.core.files.uploadedfile import SimpleUploadedFile
#from django.db.models import Q 
from django.contrib.auth.decorators import login_required
first = True
@csrf_protect
def creation(request):
	data = Image.objects.all()
	if request.method=='POST':
		form_user = UserForm(request.POST)
		if form_user.is_valid():
			user = form_user.save(commit=False)
			user.image= Image.objects.get(imageid = form_user.cleaned_data['image_number'])
			user.save()
			return HttpResponseRedirect('/login/')
		return render(request,'error.html',{'form':form_user})
	else:
		form_user = UserForm()

	return render(request,'usercreation.html',{'form':form_user,'data':data})

def success(request):
	return render(request,'success.html')

def imageupload(request):
	images = Image.objects.all()
	if request.method=='POST':
		form_image = ImageuploadForm(request.POST,request.FILES)
		if form_image.is_valid():
			new_pic = Image(imagefile=request.FILES['imagefile'])
			new_pic.save()
			return HttpResponseRedirect('/registration/')
		else:
			return render(request,'error.html',{'form':form_image})
	else:
		form_image = ImageuploadForm()
	return render(request,'image_upload.html',{'form':form_image,'data':images})

def first(request):
	if request.method=='POST':
		form_imageselection = ImageselectionForm(request.POST)
		form_login=LoginForm()
		if form_imageselection.is_valid():
			username=form_imageselection.cleaned_data['username']
			user = User.objects.get(username=username)
			data = Image.objects.filter(user__username=username)
			return render(request,'login.html',{'form':form_login,'data':data})
		else:
			return render(request,'error.html',{'form':form_imageselection})
	else:
		form_imageselection=ImageselectionForm()
	
	return render(request,'first.html',{'form':form_imageselection})

def login(request):
	
	if request.method=='POST':
		form_login = LoginForm(request.POST)

		if form_login.is_valid():
			n = User.objects.get(username=form_login.cleaned_data['username'])
			n.newsen = form_login.cleaned_data['newsen']
			n.save()
			if (form_login.cleaned_data['editpub']):
				form_editpublish = EditPublishForm(n)
				return render(request,'edit_login.html',{'form':form_editpublish})
			else:
				return HttpResponseRedirect('/publish/')
		else:
			return render(request,'error.html',{'form':form_login})
	else:
		form_login=LoginForm()
	
	return render(request,'login.html',{'form':form_login})

def editpublish(request):
	
	if request.method=='POST':
		form_editsubmit = EditSubmitForm(request.POST)

		if form_editsubmit.is_valid():
			m = form_editsubmit.save(commit=False)
			user=User.objects.get(publication__title=form_editsubmit.cleaned_data['title'])
			m.author=user
			m.save()
			return HttpResponseRedirect('/listing/')
		else:
			return render(request,'error.html',{'form':form_editsubmit})
	else:
		form_editsubmit=EditSubmitForm()
	
	return render(request,'edit_login.html',{'form':form_editsubmit})

def publication(request):
	if request.method=='POST':
		form_publish = PublishForm(request.POST)
		if form_publish.is_valid():
			if User.objects.filter(username=form_publish.cleaned_data['username']).exists():
				n = User.objects.get(username=form_publish.cleaned_data['username'])
				pub = form_publish.save(commit=False)
				pub.author = n
				pub.save()
			else:
				return HttpResponseRedirect('/login/')
			return HttpResponseRedirect('/publish/')
		return render(request,'error.html',{'form':form_publish})
	else:
		form_publish = PublishForm()
	return render(request,'publish.html',{'form':form_publish})


def listing(request):
	p = Publication.objects.select_related("author").all()
	return render(request,'listing.html',{'data':p})

def penalty(request):
	if request.method=='POST':
		form_penalty = PenaltyForm(request.POST)
		if form_penalty.is_valid():
			form_penalty.save()
			return HttpResponseRedirect('/login/')
		else:
			return render(request,'error.html',{'form':form_penalty})
	else:
		form_penalty = PenaltyForm()
	return render(request,'penalty.html',{'form':form_penalty})
