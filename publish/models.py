from __future__ import unicode_literals

from django.db import models
from django.utils import timezone


# Create your models here.


class Image(models.Model):
	imageid = models.AutoField(primary_key=True) 
	imagefile = models.ImageField(upload_to='image/%Y/%m/%d')

class User(models.Model):
	userid = models.AutoField(primary_key=True) 
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=30)
	username = models.CharField(max_length=30)
	email = models.EmailField()
	image = models.ForeignKey(Image)
	sentence = models.CharField(max_length=150)
	newsen = models.CharField(max_length=150)
	status = models.IntegerField(default=0)
	creationdate = models.DateTimeField(editable=False)

	def save(self,*args,**kwargs):
		if not self.creationdate:
			self.creationdate = timezone.now()
		super(User,self).save(*args,**kwargs)

class Publication(models.Model):
	publishid = models.AutoField(primary_key=True) 
	author = models.ForeignKey(User,on_delete=models.CASCADE)
	title = models.CharField(max_length=254)
	shortnote = models.TextField()
	keyword = models.CharField(max_length=100)
	published = models.BooleanField()
	link = models.URLField(blank=True)

class Penalty(models.Model):
	thresholdid = models.AutoField(primary_key=True)
	threshold = models.PositiveIntegerField()

class Sentence_history(models.Model):
	username = models.CharField(max_length=30)
	newsen = models.CharField(max_length=150)



