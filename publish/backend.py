from publish.models import User,Penalty
from django.contrib.auth.backends import ModelBackend
from algo2 import *


class Myauthentication(ModelBackend):

    
    
    def authenticate(self, username=None, auth_sentence=None):
        Penalty_Threshold = Penalty.objects.last()
        try:
            user = User.objects.get(username=username)
            #print "original auth_sen::"+user.sentence
            #print "recalled auth_sen::"+auth_sentence
            Total_Penalty = Main(user.sentence,auth_sentence).main()
            #print "total penalty::"+str(Total_Penalty)
            #print "Penalty threshold::"+ str(Penalty_Threshold.threshold)
            if Total_Penalty<=Penalty_Threshold.threshold:
                return user
            else:
                return None

        except User.DoesNotExist:
            return None

    # Required for your backend to work properly - unchanged in most scenarios
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None