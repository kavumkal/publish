from django.forms import ModelForm
from publish.models import Image,User,Publication,Penalty,Sentence_history
from django import forms
import backend
from backend import Myauthentication
from django.utils.translation import ugettext_lazy as _
from django.forms.models import inlineformset_factory


class UserForm(ModelForm):

	image_number = forms.IntegerField()

	class Meta:
		model = User
		fields = ['first_name','last_name','username','email','image_number','sentence']

	def clean(self):
		cleaned_data=super(UserForm, self).clean()
		username = self.cleaned_data['username']
		if User.objects.filter(username=cleaned_data.get('username')).exists():
			raise forms.ValidationError(_("The username already exists. Please try another one."),code='invalid')
		return cleaned_data

class LoginForm(ModelForm):

	editpub = forms.BooleanField( required=False)

	def __init__(self,*args,**kwargs):
		super(ModelForm,self).__init__(*args,**kwargs)
		self.fields['newsen'].label="Authentication Sentence"
		self.fields['editpub'].label="Edit Publication"

	class Meta:
		model = User
		fields = ['username','newsen']

	def clean(self):
		cleaned_data = super(LoginForm, self).clean()
		#print cleaned_data
		username = self.cleaned_data['username']
		newsen = self.cleaned_data['newsen']
		sent_his = Sentence_history(username=username,newsen=newsen)
		sent_his.save()
		user = Myauthentication().authenticate(username=username,auth_sentence=newsen)
		if user:
			return self.cleaned_data
		else:
			 raise forms.ValidationError('please enter valid username or Auth sentence')
		

class PublishForm(ModelForm):
	username=forms.CharField(max_length=30)
	class Meta:
		model = Publication
		exclude = ['author']
	
	def clean(self):
		cleaned_data = super(PublishForm,self).clean()
		published = self.cleaned_data['published']
		link = self.cleaned_data['link']
		if published and link=='':
			raise forms.ValidationError(_("Link can not be blank for published work"))
		else:
			return self.cleaned_data


class ImageuploadForm(ModelForm):
	class Meta:
		model = Image
		fields = '__all__'

class ImageselectionForm(ModelForm):
	class Meta:
		model = User
		fields = ['username']

	def clean(self):
		cleaned_data = super(ImageselectionForm, self).clean()
		if User.objects.filter(username=cleaned_data.get('username')).exists():
			return cleaned_data
		else:
			raise forms.ValidationError(_("The username do not exist,Please enter again"))

class PenaltyForm(ModelForm):

	class Meta:
		model = Penalty
		fields = ['threshold']

	def clean(self):
		cleaned_data = super(PenaltyForm,self).clean()
		threshold = self.cleaned_data['threshold']
		
		if threshold>10:
			raise forms.ValidationError(_("Threshold can not be above 10,it is between 0 and 10"))
		else:
			return self.cleaned_data


class EditPublishForm(ModelForm):
	
	def __init__(self,user, *args, **kwargs):
		super(EditPublishForm, self).__init__(*args, **kwargs)
		self.fields['title'] = forms.ModelChoiceField(queryset=Publication.objects.filter(author=user).values_list('title',flat=True))
	
	class Meta:
		model = Publication
		exclude = ['author']
	
class EditSubmitForm(ModelForm):

	class Meta:
		model = Publication
		exclude = ['author']


PublishFormset = inlineformset_factory(User,Publication,can_delete=False,fields=('title','shortnote','keyword','published','link'))