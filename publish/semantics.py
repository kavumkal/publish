import nltk
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk import pos_tag
from nltk import RegexpParser
from nltk.tag import untag
from nltk.stem import WordNetLemmatizer
from nltk.stem import PorterStemmer
from nltk.corpus import wordnet as wn

stopwords = stopwords.words('english')
grammar = """NP: {<DT>?<JJ>*<NN.*>*}
			P:{<IN>}
			V:{<VB.*>}
			PP:{<P><NP>}
			VP:{<V><NP|PP>*}"""
parser = RegexpParser(grammar)
lemmatizer_words = WordNetLemmatizer()
stemmer_words = PorterStemmer()

class Sent_Parser:
	"""
	Parse the sentences
	"""
	def __init__(self,text):
		self.text = text
		self.count = 0

	def process(self):
		"""
		tokenize, removal of stop words and pos tagging of sentence
		"""
		tokens = word_tokenize(self.text)
		tags = pos_tag(tokens)
		content = [t for t in tags if t[0] not in stopwords]
		return content
	def parser(self,tags):
		"""
		Parse sentence based on the grammar and return parse tree
		"""
		parser = RegexpParser(grammar)
		tree = parser.parse(tags)
		return tree
			
	def count_words(self,tags):
		"""
		return number of words in tagged sentence
		"""
		return len(tags)
	
	def get_pos(self,tags,pos):
		"""
		return a list of words with 'pos' tag
		"""
		pos_list = untag(filter(lambda x:x[1].startswith(pos),tags))
		return pos_list


class Compare:
	"""
	Compare original sentence with supplied sentence
	"""
	def __init__(self,tree1,tree2):
		self.tree1 = tree1
		self.tree2 = tree2
		self.noun = []
		self.verb = []
	def noun_compare(self,count_np):
		"""
		Compare noun phrases in original sentence with supplied sentence 
		and return mismatches. Mismatches in tags are also accounted for
		""" 
		for t in self.tree2.subtrees(lambda t:t.label()=='NP'):
			x = 0
			for u in self.tree1.subtrees(lambda u:u.label()=='NP'):
				if t != u: 
					x = x+1
			if x==count_np:
				self.noun.append(t)
		return self.noun

	def verb_compare(self,count_vp):
		"""
		Compare verb phrases in original sentence with supplied sentence 
		and return mismatches.Mismatches in tags are also accounted for
		"""

		for t in self.tree2.subtrees(lambda t:t.label()=='VP'):
			x = 0
			for u in self.tree1.subtrees(lambda u:u.label()=='VP'):
				if t != u:
					x = x+1
			if x==count_vp:
				self.verb.append(t)
		return self.verb
	

class Similarity:
	"""
	return similarity score of supplied words based on path_similarity algorith
	"""

	def __init__(self,word1,word2,pos):
		self.word1=wn.morphy(word1.lower(),pos)
		self.word2=wn.morphy(word2.lower(),pos)
		self.pos=pos
	def check_similarity(self):
		synset1 = filter(lambda x:x.name().__contains__(self.word1),wn.synsets(self.word1,self.pos))
		synset2 = filter(lambda x:x.name().__contains__(self.word2),wn.synsets(self.word2,self.pos))
		similarity=0
		if len(synset1)!=0 and len(synset2)!=0:
			similarity+=wn.path_similarity(synset1[0],synset2[0])
			print similarity
		return similarity


class Original:
	"""
	return string of original words from supplied sentence by doing a lookup
	"""

	def __init__(self,tags1,tags2):
		self.tags1 = tags1
		self.tags2 = tags2

	def get_orginalwords(self):
		k=map(lambda x: stemmer_words.stem(x[0]),self.tags1)
		n=filter(lambda x:stemmer_words.stem(x[0]) in k,self.tags2)
		return ' '.join([x[0] for x in n])



class Main:


	def create_tree(text):
		sent_parsed = Sent_Parser(text)
		sent_tags = sent_parsed.process()
		word_count = sent_parsed.count_words(sent_tags)
		tree = sent_parsed.parser(sent_tags)
		list_noun = sent_parsed.get_pos(sent_tags,'NN')
		list_verb = sent_parsed.get_pos(sent_tags,'VB')
		return tree,sent_tags,list_noun,list_verb,word_count

	def similarity_score(pos_list1,pos_list2,pos):
		score = 0
		for i in range(0,len(pos_list1)):
			for j in range(0,len(pos_list2)):
				if(i==j):
					if(pos_list1[i]==pos_list2[j]):
						score +=1
					else:
						sim = Similarity(pos_list1[i],pos_list2[j],pos)
						score += sim.check_similarity()
		return score

	def count_phrases(tree,label):
		count = 0
		for t in tree.subtrees(lambda t:t.label()==label):
			count+=1
		return count
	

	text1 = "Tunnel through the Himalayas"
	text2 = "digging Tunnel through the Himalayas"
	tree1,sent_tags1,list_noun1,list_verb1,word_count1 = create_tree(text1)
	tree2,sent_tags2,list_noun2,list_verb2,word_count2 = create_tree(text2)
	count1_np = count_phrases(tree1,'NP')
	count1_vp = count_phrases(tree1,'VP')
	#tree1.draw()
	#tree2.draw()


	def compare_values(value1,value2,similarity_score,list_noun1,list_noun2,list_verb1,list_verb2):
		if value1==0 and value2==0:
			print 'authenticate'
		elif value1>0 and value2==0:
			score_original = len(list_noun1)
			score = similarity_score(list_noun1,list_noun2,'n')
			if score==score_original:
				print 'authenticate'
			else:
				print 'fail'
		elif value1==0 and value2>0:
			score_original = len(list_verb1)
			score = similarity_score(list_verb1,list_verb2,'v')
			if score==score_original:
				print 'autheticate'
			else:
				print 'fail'
		else:
			print 'fail'
			exit()

	if word_count2 == word_count1:
		comparison = Compare(tree1,tree2)
		comparison.noun_compare(count1_np)
		comparison.verb_compare(count1_vp)
		#print comparison.noun
		#print comparison.verb
		compare_values(len(comparison.noun),len(comparison.verb),similarity_score,
			list_noun1,list_noun2,list_verb1,list_verb2)
		
	elif word_count2 > word_count1:
		original = Original(sent_tags1,sent_tags2)
		new_sent = original.get_orginalwords()
		tree3,sent_tags3,list_noun3,list_verb3,word_count3 = create_tree(new_sent)
		#tree3.draw()
		comparison = Compare(tree1,tree3)
		comparison.noun_compare(count1_np)
		comparison.verb_compare(count1_vp)
		#print comparison.noun
		#print comparison.verb
		compare_values(len(comparison.noun),len(comparison.verb),similarity_score,
			list_noun1,list_noun3,list_verb1,list_verb3)
	else:
		print 'fail'
		exit()



if __name__=='__main__':Main()

